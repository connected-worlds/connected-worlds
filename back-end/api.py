import falcon
from falcon.http_status import HTTPStatus
import json
import os.path as path
from csv_parser import parser_core
from urllib.parse import parse_qs
from sheets_wrapper import get as get_spreadsheet
from sheets_wrapper import build_path


"""
    To start running this service all you need to do is install gunicorn with:
    `pip install gunicorn` and then run this service with `gunicorn api:app`
"""


class HandleCORS(object):
    def process_request(self, req, resp):
        resp.set_header('Access-Control-Allow-Origin', '*')
        resp.set_header('Access-Control-Allow-Methods', '*')
        resp.set_header('Access-Control-Allow-Headers', '*')
        resp.set_header('Access-Control-Max-Age', 1728000)  # 20 days
        if req.method == 'OPTIONS':
            raise HTTPStatus(falcon.HTTP_200, body='\n')


class StatusResource(object):
    """Displays if the backend is working"""

    def on_get(self, req, resp):
        """Handles GET requests"""

        resp.status = falcon.HTTP_200
        resp.body = "I am hard at work"


class SheetsDataResource(object):
    """Retrieves Google sheet"""

    def on_get(self, req, resp):
        """Handles GET requests"""

        # Check params
        if "QUERY_STRING" not in req.env:
            resp.status = falcon.HTTP_400
            resp.body = "No query parameters were provided"
            return
        params = parse_qs(req.env["QUERY_STRING"])
        if "id" not in params:
            resp.status = falcon.HTTP_400
            resp.body = "No id parameter was provided"
        if "version" not in params:
            params["version"] = "new"

        if params["version"][0] == "new" or not path.isdir("data/sheets/{}".format(params["id"][0])):
            # Get the sheet if we want the latest version or a version has not already been downloaded
            get_spreadsheet(params["id"][0])
            parser_core.parse_spreadsheet(params["id"][0])

        # Response Construction
        try:
            with open("data/outputs/sheet/{}/output.json".format(params["id"][0]), "r") as f:
                datastore = json.load(f)
            resp.body = json.dumps(datastore)
            resp.status = falcon.HTTP_200
        except Exception as e:
            resp.status = falcon.HTTP_500
            resp.body = "An error occurred during response construction:\n" + str(e) + "\n"


class DefaultNodesResource(object):
    """Get the node data from the csv file"""

    def on_get(self, req, resp):
        """Handles GET requests"""

        # CSV Validation
        try:
            resp.status = falcon.HTTP_200
        except Exception as e:
            resp.status = falcon.HTTP_500
            resp.body = "An error occurred during format validation:\n" + str(e) + "\n"

        # CSV Parsing
        try:
            parser_core.main()
            resp.status = falcon.HTTP_200
        except Exception as e:
            resp.status = falcon.HTTP_500
            resp.body = "An error occurred during parsing:\n" + str(e) + "\n"

        # Response Construction
        try:
            with open("data/default_nodes/output/output.json", "r") as f:
                datastore = json.load(f)
            resp.body = json.dumps(datastore)
            resp.status = falcon.HTTP_200
        except Exception as e:
            resp.status = falcon.HTTP_500
            resp.body = "An error occurred during response construction:\n" + str(e) + "\n"


class DefaultYearsResource(object):
    """Get the years data from the csv file"""

    def on_get(self, req, resp):
        """Handles GET requests"""

        # CSV Validation
        try:
            resp.status = falcon.HTTP_200
        except Exception as e:
            resp.status = falcon.HTTP_500
            resp.body = "An error occurred during format validation:\n" + str(e) + "\n"

        # CSV Parsing
        try:
            parser_core.main()
            resp.status = falcon.HTTP_200
        except Exception as e:
            resp.status = falcon.HTTP_500
            resp.body = "An error occurred during parsing:\n" + str(e) + "\n"

        # Response Construction
        try:
            with open("data/default_nodes/output/years.json", "r") as f:
                datastore = json.load(f)
            resp.body = json.dumps(datastore)
            resp.status = falcon.HTTP_200
        except Exception as e:
            resp.status = falcon.HTTP_500
            resp.body = "An error occurred during response construction:\n" + str(e) + "\n"


def create():
    # Initialise the API
    app = falcon.API(middleware=[HandleCORS()])

    # Add the Nodes Resource
    nodes = DefaultNodesResource()
    app.add_route("/default/nodes", nodes)

    # Add the Years Resource
    years = DefaultYearsResource()
    app.add_route("/default/years", years)

    status = StatusResource()
    app.add_route("/status", status)

    sheets = SheetsDataResource()
    app.add_route("/sheets", sheets)

    return app


app = create()
