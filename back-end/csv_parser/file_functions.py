import csv
from os import listdir
from os.path import isfile, join


def getFileNames(dir):
    specialFiles = getFileNamesFromDirectory(join(dir, "specialNodes"))
    nodeFiles = getFileNamesFromDirectory(join(dir, "nodes"))
    nodeFiles = [fileName for fileName in nodeFiles if fileName != "roles.csv"]
    return specialFiles, nodeFiles


def extractFileIntoList(file, path):
    with open(join(path, file), "r") as f:
        reader = csv.reader(f)
        instances = list(reader)
    return instances[1:], instances[0]


def generateOutputFile(elements):
    path = "./output.json"
    jsonFile = open(path, "w+")
    jsonFile.write(elements)
    jsonFile.close()


def getFileNamesFromDirectory(dir):
    onlyfiles = [f for f in listdir(dir) if isfile(join(dir, f))]
    return onlyfiles


def getRawLineFromFile(file, path):
    with open(path + file, "r") as f:
        lines = f.readlines()
    return lines
