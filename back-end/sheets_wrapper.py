from os import getenv, mkdir
from os.path import isdir
import sys
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
import pprint
import csv
from pathlib import Path
import logging

global logger
logger = logging.getLogger("sheets_helper")
logger.setLevel(logging.DEBUG)


def auth():
    """Create a Google Sheets Service using the API Key"""
    logger.info("Creating Authentication")
    return build("sheets", "v4", developerKey=getenv("GS_SECRET"))


def build_path(path):
    """Ensure that a path is build in order to write files there"""
    logger.info("Building Path: " + str(path))
    folders = str(path).split("/")
    for i in range(len(folders)):
        if ".csv" in folders[i]:
            break
        cur_path = "/".join(folders[0 : i + 1])
        if not isdir(cur_path):
            logger.debug('MakeDir "' + cur_path + '"')
            mkdir(cur_path)


def save(tabs, spreadsheetId):
    """Save the tab contents to .csv files"""
    logger.info("Saving to csv files")
    # Save the files
    for tab in tabs:
        logger.info("Saving " + tab.get("range"))
        if tab.get("values") is None:
            logger.error("Tab contained no data")
            raise NoDataException("Tab Contained No Data")
        else:
            # Get the name of the file
            file_name = tab.get("range")
            file_name = file_name.split("!")[0]
            file_name = file_name.replace("'", "")
            file_name = file_name.replace("_", "/")
            file_name = "data/sheets/{}/{}.csv".format(spreadsheetId, file_name)
            path = Path(file_name)
            build_path(path)
            with open(path, "w+") as csv_file:
                logger.info("Writing csv file: " + str(path))
                csv_writer = csv.writer(csv_file, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL)
                values = tab.get("values")
                for row in values:
                    csv_writer.writerow(row)


def tabs(service, spreadsheetId):
    """Get all of the tabs on the spreadsheet"""
    logger.info("Getting spreadsheet")
    sheet_metadata = service.spreadsheets().get(spreadsheetId=spreadsheetId).execute()
    sheets = sheet_metadata.get("sheets")

    # A list of tabs by mapping each tab of the metadata to tab['properties']['title']
    logger.info("Getting tabs")
    tab_names = list(map(lambda tab: tab["properties"]["title"], sheets))

    tabs = []
    for tab_name in tab_names:
        tab = service.spreadsheets().values().get(spreadsheetId=spreadsheetId, range=tab_name).execute()
        tabs.append(tab)

    # Return a list where each element is the contents of a tab
    return tabs


def get(spreadsheetId):
    """Use a provided Google Sheets ID to save its contents to .csv files"""
    logger.info("Getting spreadsheet: " + str(spreadsheetId))
    service = auth()
    allTabs = tabs(service, spreadsheetId)
    save(allTabs,spreadsheetId)


def main():
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    # Public
    # get("1Pkx-4fJsFn43n7LclzJL6QnDNe8FnK1tIBPVnRbB5_Q")
    # Private
    # get("1WhpfABAJgZbr-sGp5VxuNd3xBpqaRKadKQhPPbvpBcw")
    # Example
    # get("1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms")
    # Existing
    get("19_Ed5XN3AY9IQXyO1iF4E5-EBS4R1VDB0f8vvetBMSE")


class NoDataException(Exception):
    pass


if __name__ == "__main__":
    main()
