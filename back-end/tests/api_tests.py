from falcon import testing
import json
from api import create
from shutil import rmtree
from os import rmdir

"""
    Run these tests with the following command from `parser/`:
    `python3 -m unittest tests.api_tests -v`
"""


class DefaultTests(testing.TestCase):
    def setUp(self):
        super(DefaultTests, self).setUp()
        self.app = testing.TestClient(create())

    def test_get_default_years(self):
        result = self.app.simulate_get("/default/years")
        assert result.status_code == 200

        with open("data/default_nodes/output/years.json", "r") as f:
            expected = json.load(f)
        assert result.json == expected

    def test_get_default_nodes(self):
        result = self.app.simulate_get("/default/nodes")
        assert result.status_code == 200

        with open("data/default_nodes/output/output.json", "r") as f:
            expected = json.load(f)
        assert result.json == expected

    def test_get_status(self):
        result = self.app.simulate_get("/status")
        assert result.status_code == 200


class SheetsTest(testing.TestCase):
    def setUp(self):
        super(SheetsTest, self).setUp()
        self.app = testing.TestClient(create())

    def test_get_example_sheet(self):

        # Test initial download
        result = self.app.simulate_get("/sheets?id=19_Ed5XN3AY9IQXyO1iF4E5-EBS4R1VDB0f8vvetBMSE")
        assert result.status_code == 200

        # Test retrieving a cached version
        result = self.app.simulate_get("/sheets?id=19_Ed5XN3AY9IQXyO1iF4E5-EBS4R1VDB0f8vvetBMSE&version=cached")

        # Clean up the created files
        rmtree("data/sheets")

    

        # This will need to remain commented out until we can change the parser so that the produced json
        # document is consistant
        # with open("data/testData/19_Ed5XN3AY9IQXyO1iF4E5-EBS4R1VDB0f8vvetBMSE/output.json", "r") as f:
        #     expected = json.load(f)
        # assert result.json == expected
