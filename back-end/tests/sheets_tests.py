import unittest
import sheets_wrapper
import pprint as pp
from os.path import isdir
from os import rmdir, listdir
import filecmp

# This class tests all of the helper functions used by the spreadsheet wrapper
class UtilTests(unittest.TestCase):

    def test_auth(self):
        # This sheet is a public sheet hosted by google
        sheetID = "1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms"
        service = sheets_wrapper.auth()
        # Executing this command will check that the API key is valid
        sheet_metadata = service.spreadsheets().get(spreadsheetId=sheetID).execute()
        # This will check that the sheet service passed the correct values back
        self.assertEqual(sheet_metadata['spreadsheetId'], sheetID)

    def test_path_builder(self):
        # Check the path doesn't exist before begining
        self.assertFalse(isdir("testPath"))
        # Create the path
        sheets_wrapper.build_path("testPath/subFolder/subsubFolder")
        # Test it was created
        self.assertTrue(isdir("testPath/subFolder/subsubFolder"))
        # Remove the created folders
        rmdir("testPath/subFolder/subsubFolder")
        rmdir("testPath/subFolder")
        rmdir("testPath")

    def test_tabs(self):
        # This sheet is a public sheet hosted by google
        sheetID = "1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms"
        # This test is dependent on the service test passing
        service = sheets_wrapper.auth()
        # Actually retrieve the tabs
        tabs = sheets_wrapper.tabs(service, sheetID)
        # Retrieved from the google sheet
        expectedValues = {'range': "'Class Data'!A1:V101", 'majorDimension': 'ROWS', 'values': [['Student Name', 'Gender', 'Class Level', 'Home State', 'Major', 'Extracurricular Activity'], ['Alexandra', 'Female', '4. Senior', 'CA', 'English', 'Drama Club'], ['Andrew', 'Male', '1. Freshman', 'SD', 'Math', 'Lacrosse'], ['Anna', 'Female', '1. Freshman', 'NC', 'English', 'Basketball'], ['Becky', 'Female', '2. Sophomore', 'SD', 'Art', 'Baseball'], ['Benjamin', 'Male', '4. Senior', 'WI', 'English', 'Basketball'], ['Carl', 'Male', '3. Junior', 'MD', 'Art', 'Debate'], ['Carrie', 'Female', '3. Junior', 'NE', 'English', 'Track & Field'], ['Dorothy', 'Female', '4. Senior', 'MD', 'Math', 'Lacrosse'], ['Dylan', 'Male', '1. Freshman', 'MA', 'Math', 'Baseball'], ['Edward', 'Male', '3. Junior', 'FL', 'English', 'Drama Club'], ['Ellen', 'Female', '1. Freshman', 'WI', 'Physics', 'Drama Club'], ['Fiona', 'Female', '1. Freshman', 'MA', 'Art', 'Debate'], ['John', 'Male', '3. Junior', 'CA', 'Physics', 'Basketball'], ['Jonathan', 'Male', '2. Sophomore', 'SC', 'Math', 'Debate'], ['Joseph', 'Male', '1. Freshman', 'AK', 'English', 'Drama Club'], ['Josephine', 'Female', '1. Freshman', 'NY', 'Math', 'Debate'], ['Karen', 'Female', '2. Sophomore', 'NH', 'English', 'Basketball'], ['Kevin', 'Male', '2. Sophomore', 'NE', 'Physics', 'Drama Club'], ['Lisa', 'Female', '3. Junior', 'SC', 'Art', 'Lacrosse'], ['Mary', 'Female', '2. Sophomore', 'AK', 'Physics', 'Track & Field'], ['Maureen', 'Female', '1. Freshman', 'CA', 'Physics', 'Basketball'], ['Nick', 'Male', '4. Senior', 'NY', 'Art', 'Baseball'], ['Olivia', 'Female', '4. Senior', 'NC', 'Physics', 'Track & Field'], ['Pamela', 'Female', '3. Junior', 'RI', 'Math', 'Baseball'], ['Patrick', 'Male', '1. Freshman', 'NY', 'Art', 'Lacrosse'], ['Robert', 'Male', '1. Freshman', 'CA', 'English', 'Track & Field'], ['Sean', 'Male', '1. Freshman', 'NH', 'Physics', 'Track & Field'], ['Stacy', 'Female', '1. Freshman', 'NY', 'Math', 'Baseball'], ['Thomas', 'Male', '2. Sophomore', 'RI', 'Art', 'Lacrosse'], ['Will', 'Male', '4. Senior', 'FL', 'Math', 'Debate']]}
        # Check the correct values were returned
        self.assertEqual(expectedValues, tabs[0])

class IntegrationTests(unittest.TestCase):

    def test_integration(self):
        # Check the folder isn't present before testing
        self.assertFalse(isdir("data/sheets/19_Ed5XN3AY9IQXyO1iF4E5-EBS4R1VDB0f8vvetBMSE"))
        # Get the connected worlds test data
        sheets_wrapper.get("19_Ed5XN3AY9IQXyO1iF4E5-EBS4R1VDB0f8vvetBMSE")
        # Check that the correct folder was created
        self.assertTrue(isdir("data/sheets/19_Ed5XN3AY9IQXyO1iF4E5-EBS4R1VDB0f8vvetBMSE"))
        # Check the years are present and include files
        self.assertTrue(isdir("data/sheets/19_Ed5XN3AY9IQXyO1iF4E5-EBS4R1VDB0f8vvetBMSE/2016"))
        self.assertTrue(len(listdir("data/sheets/19_Ed5XN3AY9IQXyO1iF4E5-EBS4R1VDB0f8vvetBMSE/2016") ) > 0)
        self.assertTrue(isdir("data/sheets/19_Ed5XN3AY9IQXyO1iF4E5-EBS4R1VDB0f8vvetBMSE/2017"))
        self.assertTrue(len(listdir("data/sheets/19_Ed5XN3AY9IQXyO1iF4E5-EBS4R1VDB0f8vvetBMSE/2017") ) > 0)
        self.assertTrue(isdir("data/sheets/19_Ed5XN3AY9IQXyO1iF4E5-EBS4R1VDB0f8vvetBMSE/2018"))
        self.assertTrue(len(listdir("data/sheets/19_Ed5XN3AY9IQXyO1iF4E5-EBS4R1VDB0f8vvetBMSE/2018") ) > 0)
        