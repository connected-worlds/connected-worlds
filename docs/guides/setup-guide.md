# Setting up connected worlds with your data using GitLab, Bitbucket or GitHub

## Contents:

- [Notes](#notes)
- [Importing Connected Worlds](#importing-connected-worlds)
    - [Importing into GitLab](#importing-into-gitlab)
- [Deploying](#deploying)
    - [Setting up your CI service](#setting-up-your-ci-service)
        - [GitLabCI](#gitlabci)
- [Using your own data](#using-your-own-data)

## Notes:
1. This guide is designed for someone with a reasonable knowledge of the chosen git services and some technical experience.
2. GitLab will use GitLab CI.
3. It is assumed that you will have all relevant accounts and all the needed permissions to follow this guide.

## Importing Connected Worlds:

Note the repository may take a few minutes to import.

### Importing into GitLab:

1. Login to GitLab.
2. Click on "New project" in the top right corner of the screen.
3. Click on "Repo by URL".
4. Click on "Import project" and then paste the following URL:

https://github.com/eResearchSandpit/VR-Network-Vis

![Importing repository](images/importing-project-gitlab.png)

5. Scroll down and rename the project if you would like.
6. Set the visibility level of the project(Private, Internal or Public).
7. Click create project.

## Deploying:

Note that when using GitLab, the CI script is set up to deploy to GitLab pages automatically.

### Setting up your CI service:

#### GitLabCI:

Pipelines are defined in `.gitlab-ci.yml`

See the CI Pipeline [wiki page](https://gitlab.ecs.vuw.ac.nz/ENGR300-2019/Project-04/connected-worlds/wikis/DevOps/CI-Pipeline)


## Using your own data:
See the [data-uploading-and-formatting document](docs/guides/data-uploading-and-formatting.md) for instructions.
