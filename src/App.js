import React, { Component } from "react";
import Cytoscape from "./components/Cytoscape";
import TopBar from "./components/TopBar";
import BottomBar from "./components/BottomBar";
import cytoscapeStore from "./util/CytoscapeStore";
import Tutorial from "./components/VideoHelp";
import StylePage from "./components/StylePage";
import CacheWarning from "./components/CacheWarning";
class App extends Component {
  state = { highlightedNode: undefined };

  constructor(props) {
    super(props);
    StylePage.parseStyles();
  }

  render() {
    return (
      <div className="App">
        <TopBar highlightedNode={this.state.highlightedNode} />
        <Cytoscape
          cytoscapeStore={cytoscapeStore}
          callbackFromParent={this.getHighlightedNode}
        />
        <BottomBar highlightedNode={this.state.highlightedNode} />
        <Tutorial />
        <CacheWarning />
      </div>
    );
  }

  getHighlightedNode = node => {
    this.setState({ highlightedNode: node });
  };
}

export default App;
