import React, { Component } from "react";

class Guide extends Component {
  render() {
    return (
      <div>
        <div className="text title">CONNECTED WORLDS</div>
        <p className="text intro">
          To get started with Connected Worlds, you'll need to have a Google
          Sheet with your data stored in it.
        </p>
        <p className="text intro">
          You can either create a blank new graph, or import an existing sheet.
        </p>
        <p className="text intro">
          If you're looking to use an existing sheet, it should follow
          consistent format, e.g.
        </p>
      </div>
    );
  }
}

export default Guide;
