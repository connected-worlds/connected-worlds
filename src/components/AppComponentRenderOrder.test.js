import React from "react";
import ReactDOM from "react-dom";
import Cytoscape from "./Cytoscape";
import TopBar from "./TopBar";
import BottomBar from "./BottomBar";
import CytoscapeStore from "../util/CytoscapeStore";
import { shallow } from "enzyme";

describe("<Cytoscape/> Tests", () => {
  //Test the rendering of Cytoscape (The nodes)
  it("Renders Cytoscape without crashing.", () => {
    const div = document.createElement("div");
    ReactDOM.render(<Cytoscape />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  //Test that the Top Bar and Bottom Bar components do not render before the Nodes.
  //This prevents the user from trying to perform navigation functions on nodes that do not exist yet.
  it("Does not render Top Bar if the nodes have not been loaded.", () => {
    const wrapper = shallow(<TopBar />);
    expect(wrapper.equals(null)).toBe(true);
  });

  it("Does not render Bottom Bar if the nodes have not been loaded.", () => {
    const wrapper = shallow(<BottomBar />);
    expect(wrapper.equals(null)).toBe(true);
  });

  //Test that once the node loading has finished,
  //the top bar and bottom bar components will successfully load.
  it("Renders Top Bar once the nodes have been loaded.", () => {
    CytoscapeStore.loadingFinished = true;
    const wrapper = shallow(<TopBar />);
    expect(wrapper.equals(null)).toBe(false);
  });

  it("Renders Bottom Bar once the nodes have been loaded.", () => {
    CytoscapeStore.loadingFinished = true;
    const wrapper = shallow(<BottomBar />);
    expect(wrapper.equals(null)).toBe(false);
  });
});
