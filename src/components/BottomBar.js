import React from "react";
import Search from "./Search";
import logo from "../assets/vic-logo.svg";
import DataSwitch from "./DataSwitch";
import { observer } from "mobx-react";
import "./BottomBar.css";
import HelpButton from "./HelpButton";
import cytoscapeStore from "../util/CytoscapeStore";

class BottomBar extends React.Component {
  render() {
    if (!cytoscapeStore.loadingFinished) {
      return null;
    }
    return (
      <div id="navbar-bottom">
        <Search />
        <div id="outer-header">
          <div id="header">
            <span>Connected Worlds</span>
            <span>: Research into VR, AR and MR at Victoria</span>
            <img src={logo} id="vuw-logo" alt="Connected Worlds Logo" />
          </div>
        </div>
        <div className="sliderContainer">
          <DataSwitch />
        </div>
        <HelpButton />
      </div>
    );
  }
}

export default observer(BottomBar);
