import React from "react";
import { observer } from "mobx-react";
import ReactNotification from "react-notifications-component";
import "react-notifications-component/dist/theme.css";
import DetectBrowser from "react-detect-browser";

class CacheWarning extends React.Component {
  constructor(props) {
    super(props);
    this.addWarnNotification = this.addWarnNotification.bind(this);
    this.addSuccessNotification = this.addSuccessNotification.bind(this);
    this.notificationDOMRef = React.createRef();
  }

  addWarnNotification() {
    this.notificationDOMRef.current.addNotification({
      title: "Error!",
      message: "if your data doesn't load, please refresh",
      type: "warning",
      insert: "top",
      container: "bottom-left",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: { duration: 10000 },
      dismissable: { click: true }
    });
  }

  addSuccessNotification() {
    this.notificationDOMRef.current.addNotification({
      title: "Success",
      message: "Retrieving cached data for us is much easier to work with",
      type: "success",
      insert: "top",
      container: "bottom-left",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: { duration: 5000 },
      dismissable: { click: true }
    });
  }

  render() {
    return (
      <DetectBrowser>
        {({ browser }) => {
          return browser ? ( //check if there is browser first
            !browser.name.includes(" ") ? ( //then display the message regardless of browser type
              <div>
                {" "}
                <ReactNotification ref={this.notificationDOMRef} />
                {(window.onload = this.addWarnNotification)}
              </div>
            ) : null
          ) : null;
        }}
      </DetectBrowser>
    );
  }
}

export default observer(CacheWarning);
