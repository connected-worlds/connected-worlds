import React from "react";
import ReactDOM from "react-dom";
import BrowserDetection from "./CacheWarning";
import { mount, find, at, simulate, text, instance } from "enzyme";

it("renders browser detection without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<BrowserDetection />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it("renders browser detection correctly without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<BrowserDetection />, div);
  expect(div).toMatchSnapshot();
  ReactDOM.unmountComponentAtNode(div);
});
