import React, { Fragment } from "react";
import "./ColourPicker.css";
import cytoscapeStore from "../util/CytoscapeStore";
import MenuButton from "./MenuButton";

class ColourPicker extends React.Component {
  constructor(props) {
    super(props);
    if (this.props.clickHandler) {
      this.buttonClicked = this.props.clickHandler;
    }
    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
    this.state = {
      showMenu: false,
      names: ["Classic", "Sunrise", "Daytime", "Sunset"]
    };
  }

  onItemSelected = name => {
    this.setState({ ...this.state, showMenu: false });
    cytoscapeStore.nodeStyles = this.state.names.indexOf(name);
    //console.log(cytoscapeStore.nodeStyles);
  };

  render() {
    let divClass = "";
    if (this.state.showMenu) {
      divClass = "menu-selected";
    }
    return (
      <div ref={this.setWrapperRef} className="control">
        <div id="drop-down-menu" className={divClass}>
          <MenuButton name="COLOUR SCHEME" onSelect={this.buttonClicked} />
        </div>
        {this.state.showMenu ? this.createMenu() : null}
      </div>
    );
  }

  createMenu = () => {
    return (
      <div className="menu menu-open menu-div">
        <ul id="menu" className="menu-ul">
          <Fragment>
            {this.state.names.map((name, index) => (
              <MenuButton
                key={index}
                id={index}
                name={name}
                onSelect={this.onItemSelected}
              />
            ))}
          </Fragment>
        </ul>
      </div>
    );
  };

  componentDidMount() {
    document.addEventListener("mousedown", this.handleClickOutside);
  }

  // noinspection SpellCheckingInspection
  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleClickOutside);
  }

  /**
   * Set the wrapper ref
   */
  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  /**
   * Alert if clicked on outside of element
   */
  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({ ...this.state, showMenu: false });
    }
  }

  buttonClicked = () => {
    this.setState({ ...this.state, showMenu: !this.state.showMenu });
  };
}
export default ColourPicker;
