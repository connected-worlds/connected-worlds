import React from "react";
import ReactDOM from "react-dom";
import DataSwitch from "./DataSwitch";
import { shallow } from "enzyme";

describe("<DataSwitch/> Tests", () => {
  //test rendering
  it("1. Renders DataSwitch without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<DataSwitch />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it("2. Renders DataSwitch correctly without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<DataSwitch />, div);
    expect(div).toMatchSnapshot();
    ReactDOM.unmountComponentAtNode(div);
  });

  //test elements
  const wrapper = shallow(<DataSwitch />);

  it('3. An element with class "wrapper" exists', () => {
    expect(wrapper.find(".wrapper").exists()).toBeTruthy();
  });

  it("4. Outer div contains 2 children elements", () => {
    expect(wrapper.find("div").children().length).toBe(2);
  });

  it("5. Contains one span component", () => {
    expect(wrapper.find("span").length).toBe(1);
  });

  it("6. The default span range is 2016 - 2018", () => {
    expect(wrapper.find("span").text()).toBe("Range: 2016 - 2018");
  });

  it("7. Test handleChange(), should change span range to 2001 - 2008", () => {
    var years = [2001, 2008];
    wrapper.instance().handleChange(years);
    expect(wrapper.instance().min).toBe(2001);
    expect(wrapper.instance().max).toBe(2008);
  });

  it("8. Test handleSelect() for name value and displayResults", () => {
    var i = { name: "Bob" };
    wrapper.instance().handleSelect(i);
    expect(wrapper.state().value).toBe("Bob");
    expect(wrapper.state().displayResults).toBe(false);
  });
});
