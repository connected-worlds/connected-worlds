import React, { Fragment } from "react";
import RadioButton from "./RadioButton";
import cytoscapeStore from "../util/CytoscapeStore";
import { observer } from "mobx-react";
import NodeInfo from "./NodeInfo";
import "./DetailsPane.css";

class DetailsPane extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isChecked: true
    };
  }

  clickHandler() {
    this.setState({
      ...this.state,
      isChecked: !this.state.isChecked
    });
  }

  infoPane(node) {
    return (
      //Checks if a node is selected and displays the info if it is
      this.state.isChecked ? (
        <div id="infoContainer" className="info">
          <div className="container">{node ? <NodeInfo /> : null}</div>
        </div>
      ) : null
    );
  }

  render() {
    const node = cytoscapeStore.visNodesMap[cytoscapeStore.selectedNode];
    const highlighted = this.props.highlightedNode;
    return (
      <div id="detailsBar">
        <div id="toggle">
          <Fragment>
            <RadioButton
              isChecked={this.state.isChecked}
              clickHandler={event => this.clickHandler(event)}
            />
            <h2>Show details</h2>
            {
              <h1
                className="nameHeader"
                style={{
                  backgroundColor: "rgba(46,51,52,0.9)",
                  padding: 7,
                  borderRadius: 15,
                  borderStyle: "solid",
                  borderColor: "white",
                  borderWidth: 0.5,
                  marginTop: 10,
                  marginRight: 0
                }}
              >
                {highlighted ? (
                  highlighted
                ) : node ? (
                  node.name
                ) : (
                  <em> Select any node to see the detail </em>
                )}
              </h1>
            }
          </Fragment>
        </div>
        <div id="nodeDetails" className="expanded">
          {this.infoPane(node)}
        </div>
      </div>
    );
  }
}

export default observer(DetailsPane);
