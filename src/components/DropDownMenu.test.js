import React from "react";
import ReactDOM from "react-dom";
import DropDownMenu from "./DropDownMenu";
import MenuButton from "./MenuButton";
import { shallow, mount } from "enzyme";

describe("<DropDownMenu/> Tests", () => {
  it("1. Renders DropDownMenu without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<DropDownMenu />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it("2. Renders DropDownMenu correctly without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<DropDownMenu />, div);
    expect(div).toMatchSnapshot();
    ReactDOM.unmountComponentAtNode(div);
  });

  const wrapper = shallow(<DropDownMenu />);

  it('3. An element with class "control" exists', () => {
    expect(wrapper.find(".control").exists()).toBeTruthy();
  });

  it("4. Renders a MenuButton", () => {
    expect(wrapper.find(MenuButton).props().name).toEqual("FOCUS TYPE");
  });

  it("5. Test buttonClicked(), it should flip showMenu", () => {
    const wrapper = shallow(<DropDownMenu />);
    expect(wrapper.state().showMenu).toBe(false);
    wrapper.setProps({ data: wrapper }); //wrapper just for setting the data, does not do anything else
    wrapper.instance().buttonClicked();
    expect(wrapper.state().showMenu).toBe(true);
  });

  it("6. Test createMenu(), it should create a div of menu", () => {
    const wrapper = shallow(<DropDownMenu />);

    wrapper.setProps({ data: wrapper }); //wrapper just for setting the data, does not do anything else
    wrapper.instance().buttonClicked(); //flip showMenu true from false so we can create menu
    wrapper.instance().createMenu();
    //check if the menu has been created
    expect(wrapper.find(".menu").exists).toBeTruthy();
    expect(wrapper.find(".menu-open").exists).toBeTruthy();
    expect(wrapper.find(".menu-div").exists).toBeTruthy();
    expect(wrapper.find(".menu-ul").exists).toBeTruthy();
  });
});

describe("<DropDownMenu/> Mount Tests", () => {
  it("1. Not showing menu when button is not clicked", () => {
    const wrapper = mount(<DropDownMenu />);
    expect(wrapper.state().showMenu).toEqual(false);
  });

  it("2. Click works", () => {
    const mockCallback = jest.fn();
    const wrapper = shallow(<DropDownMenu clickHandler={mockCallback} />);

    expect(mockCallback.mock.calls.length).toBe(0);
    wrapper.instance().buttonClicked();
    expect(mockCallback.mock.calls.length).toBe(1);
  });
});
