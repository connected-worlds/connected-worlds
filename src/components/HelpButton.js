import React, { Fragment } from "react";
import cytoscapeStore from "../util/CytoscapeStore";
import HelpMenuButton from "./HelpMenuButton";
import ZoomBackButton from "./ZoomBackButton";
import "./HelpButton.css";

class HelpButton extends React.Component {
  constructor(props) {
    super(props);
    if (this.props.clickHandler) {
      this.buttonClicked = this.props.clickHandler;
    }
    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
    this.state = {
      showMenu: false,
      names: ["Tutorial", "Report Bugs", "Request Additions", ""]
    };
  }

  svg() {
    return (
      <svg
        className="svg-path"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 512 512"
        fill="#fff"
        height="15px"
      >
        <path d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zM262.655 90c-54.497 0-89.255 22.957-116.549 63.758-3.536 5.286-2.353 12.415 2.715 16.258l34.699 26.31c5.205 3.947 12.621 3.008 16.665-2.122 17.864-22.658 30.113-35.797 57.303-35.797 20.429 0 45.698 13.148 45.698 32.958 0 14.976-12.363 22.667-32.534 33.976C247.128 238.528 216 254.941 216 296v4c0 6.627 5.373 12 12 12h56c6.627 0 12-5.373 12-12v-1.333c0-28.462 83.186-29.647 83.186-106.667 0-58.002-60.165-102-116.531-102zM256 338c-25.365 0-46 20.635-46 46 0 25.364 20.635 46 46 46s46-20.636 46-46c0-25.365-20.635-46-46-46z" />
      </svg>
    );
  }

  onHelpClick() {
    cytoscapeStore.showTutorial = true;
  }

  onItemSelected = name => {
    if (name === "Tutorial") {
      this.onHelpClick();
    }
    // Change the mailto: links here to the emails you wish to direct users to.
    if (name === "Report Bugs") {
      window.location.href =
        "mailto:gitlab-incoming+engr300-2019-project-04-connected-worlds-3823-issue-@ecs.vuw.ac.nz";
    }
    if (name === "Request Additions") {
      window.location.href =
        "mailto:gitlab-incoming+engr300-2019-project-04-connected-worlds-3823-issue-@ecs.vuw.ac.nz";
    }
    this.setState({ ...this.state, showMenu: false });
  };

  /* This code here was to do with the help button icon that was present on the button.
   * Might be useful later?
   *
  render() {
    return (
      <div className="" id={"helpButtonCss"} onClick={this.onClick}>
        Help
        <span id="img-option">{this.svg()}</span>
      </div>
    );
  }
  */
  render() {
    let divClass = "";
    if (this.state.showMenu) {
      divClass = "menu-selected";
    }
    return (
      <div ref={this.setWrapperRef} className="control">
        <div id="parentHelpButtonCss" className={divClass}>
          <HelpMenuButton
            name={"Additional Options"}
            onSelect={this.buttonClicked}
          />
          <ZoomBackButton />
        </div>
        {this.state.showMenu ? this.createMenu() : null}
      </div>
    );
  }

  createMenu = () => {
    return (
      <div className="menu menu-open menu-div">
        <ul id="helpMenu" className="menu-ul">
          <Fragment>
            {this.state.names.map((name, index) => (
              <HelpMenuButton
                key={index}
                id={index}
                name={name}
                onSelect={this.onItemSelected}
              />
            ))}
          </Fragment>
        </ul>
      </div>
    );
  };

  componentDidMount() {
    document.addEventListener("mousedown", this.handleClickOutside);
  }

  // noinspection SpellCheckingInspection
  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleClickOutside);
  }

  /**
   * Set the wrapper ref
   */
  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  /**
   * Alert if clicked on outside of element
   */
  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({ ...this.state, showMenu: false });
    }
  }

  buttonClicked = () => {
    this.setState({ ...this.state, showMenu: !this.state.showMenu });
  };
}

export default HelpButton;
