import React from "react";
import ReactDOM from "react-dom";
import HelpButton from "./HelpButton";
import { shallow } from "enzyme";

describe("<HelpButton/> Tests", () => {
  it("1. Renders HelpButton without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<HelpButton />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it("2. Renders HelpButton correctly without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<HelpButton />, div);
    expect(div).toMatchSnapshot();
    ReactDOM.unmountComponentAtNode(div);
  });

  const wrapper = shallow(<HelpButton />);

  it('3. Svg element with class name "control" exists ', () => {
    expect(wrapper.find(".control").exists()).toBeTruthy();
  });

  it("4. One HelpMenuButton tag exists", () => {
    expect(wrapper.find("HelpMenuButton").length).toBe(1);
  });

  it("5. buttonClicked() should flip showMenu to true at the first time", () => {
    wrapper.instance().buttonClicked();
    expect(wrapper.state().showMenu).toBe(true);
  });

  it("6. buttonClicked() should flip showMenu ", () => {
    const wrapper = shallow(<HelpButton />); //create a seperate wrapper
    wrapper.instance().buttonClicked();
    expect(wrapper.state().showMenu).toBe(true);
    wrapper.instance().buttonClicked();
    expect(wrapper.state().showMenu).toBe(false);
  });

  it('7. onItemSelected() should turn showMenu to false if the name is not "Tutorial" ', () => {
    const wrapper = shallow(<HelpButton />);
    wrapper.instance().buttonClicked(); //flip showMenu to true
    expect(wrapper.state().showMenu).toBe(true);
    wrapper.instance().onItemSelected("text"); //turn showMenu to false
    expect(wrapper.state().showMenu).toBe(false);
  });
});
