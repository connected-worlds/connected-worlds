import React from "react";
import ReactDOM from "react-dom";
import MenuButton from "./MenuButton";
import { shallow } from "enzyme";

describe("<MenuButton/> Tests", () => {
  it("1. Renders MenuButton without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<MenuButton />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it("2. Renders MenuButton correctly without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<MenuButton />, div);
    expect(div).toMatchSnapshot();
    ReactDOM.unmountComponentAtNode(div);
  });

  it("3. Renders MenuButton with name Projects without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<MenuButton name="Projects" />, div);
    expect(div).toMatchSnapshot();
    ReactDOM.unmountComponentAtNode(div);
  });

  const wrapper = shallow(<MenuButton />);

  it('4. An element with class "button-css" exists', () => {
    expect(wrapper.find(".button-css").exists()).toBeTruthy();
  });
});
