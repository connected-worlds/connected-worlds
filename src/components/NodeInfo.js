import React, { Fragment } from "react";
import cytoscapeStore from "../util/CytoscapeStore";
import idImg from "../assets/id-img.png";
import schoolImg from "../assets/vic-logo.svg";
import "./NodeInfo.css";

class NodeInfo extends React.Component {
  render() {
    const node = cytoscapeStore.visNodesMap[cytoscapeStore.selectedNode];
    return <Fragment>{this.nodeData(node)}</Fragment>;
  }

  nodeData(node) {
    const bio = node.bio;
    const programme = node.programme;
    const infoSchool = node.fields.school ? node.fields.school[0] : null;
    let mediaLink = node.mediaLink;
    const siteLink = node.siteLink;
    const staffSiteLink = node.staffSiteLink;
    const siteName = !node.siteName ? node.name : node.siteName;
    const nodeType = node.type;
    const role = node.role;
    const datesActive = node.datesActive;
    const tags = node.tags;
    if (nodeType === "person" && !mediaLink) {
      mediaLink = idImg;
    } else if (nodeType === "school" && !mediaLink) {
      mediaLink = schoolImg;
    }
    const img = new Image();
    img.src = mediaLink;
    if (img.height === 0) {
      if (nodeType === "person") {
        mediaLink = idImg;
      } else if (nodeType === "school") {
        mediaLink = schoolImg;
      }
    }

    return (
      <Fragment>
        {this.parseMedia(mediaLink, staffSiteLink)}
        {this.parseRole(role)}
        {this.parseProgramme(programme)}
        {this.parseSchool(infoSchool)}
        {this.parseSite(siteName, siteLink, staffSiteLink)}
        {this.parseDates(datesActive)}
        {this.parseTags(tags)}
        {this.parseBio(bio)}
      </Fragment>
    );
  }

  parseMedia(mediaLink, staffSiteLink) {
    const pattern1 = /(?:http?s?:\/\/)?(?:www\.)?(?:vimeo\.com)\/?(.+)/g;
    const pattern2 = /(?:http?s?:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+)/g;

    if (pattern1.test(mediaLink)) {
      const replacement = "https://player.vimeo.com/video/$1";
      const link = mediaLink.replace(pattern1, replacement);
      return (
        <div className="videoWrapper">
          <iframe
            title="Youtube Video"
            width="1920"
            height="1080"
            className="info-media"
            src={link}
            frameBorder="0"
            allowFullScreen
          />
        </div>
      );
    }

    if (pattern2.test(mediaLink)) {
      const replacement =
        "https://www.youtube.com/embed/$1?&rel=0&showinfo=0&modestbranding=1&hd=1&autohide=1&color=white";
      const link = mediaLink.replace(pattern2, replacement);
      return (
        <div className="videoWrapper">
          <iframe
            title="Vimeo Video"
            width="1920"
            height="1080"
            className="info-media"
            src={link}
            frameBorder="0"
            allowFullScreen
          />
        </div>
      );
    }

    return (
      <div className="id-wrapper id-linked">
        <a href={staffSiteLink} target="_blank" rel="noopener noreferrer">
          <img
            className="img-crop"
            src={mediaLink}
            alt={"Digital Portrait or descriptive media link"}
          />
        </a>
      </div>
    );
  }

  parseRole(role) {
    return role ? (
      <div className="info-row">
        <p className="info-left">Role</p>
        <p className="info-right">{this.capitaliseRole(role)}</p>
      </div>
    ) : null;
  }

  capitaliseRole(role) {
    return role
      .split(" ")
      .map(s => s.charAt(0).toUpperCase() + s.substring(1))
      .join(" ");
  }

  parseSchool(infoSchool) {
    return infoSchool ? (
      <div className="info-row">
        <p className="info-left">School</p>
        <p className="info-right">{infoSchool}</p>
      </div>
    ) : null;
  }

  parseProgramme(programme) {
    return programme ? (
      <div className="info-row">
        <p className="info-left">Programme</p>
        <p className="info-right">{programme}</p>
      </div>
    ) : null;
  }

  parseSite(siteName, siteLink, staffSiteLink) {
    return (
      <Fragment>
        {siteLink ? this.parseLink(siteName, siteLink) : null}
        {staffSiteLink ? this.parseStaffLink(siteName, staffSiteLink) : null}
      </Fragment>
    );
  }

  parseLink(siteName, siteLink) {
    return (
      <div className="info-row">
        <p className="info-left">Website</p>
        <p className="info-right">
          <a href={siteLink}>{siteName}</a>
        </p>
      </div>
    );
  }

  parseStaffLink(siteName, staffSiteLink) {
    return (
      <div className="info-row">
        <p className="info-left">Staff Webpage</p>
        <p className="info-right">
          <a href={staffSiteLink}>{siteName}</a>
        </p>
      </div>
    );
  }

  parseDates(datesActive) {
    return datesActive && datesActive !== "placeholder" ? (
      <div className="info-row">
        <p className="info-left">Dates Active</p>
        <p className="info-right">{datesActive}</p>
      </div>
    ) : null;
  }

  parseTags(tags) {
    if (tags) tags = tags.replace(",", ", ");
    else tags = "None";
    return (
      <div className="info-row">
        <p className="info-left">Tags</p>
        <p className="info-right">{tags}</p>
      </div>
    );
  }

  parseBio(bio) {
    return bio ? (
      <div className="info-row">
        <hr />
        <p className="info-bio">{bio}</p>
      </div>
    ) : null;
  }
}

export default NodeInfo;
