import React from "react";
import ReactDOM from "react-dom";
import Search from "./Search";
import { shallow, mount } from "enzyme";

describe("<Search/> Tests", () => {
  it("1. Renders Search without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<Search />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it("2. Renders Search correctly without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<Search />, div);
    expect(div).toMatchSnapshot();
    ReactDOM.unmountComponentAtNode(div);
  });

  const wrapper = shallow(<Search />);

  it("3. Search must be text ", () => {
    expect(wrapper.find("input").props().type).toBe("text");
  });

  it("4. handleSelect() should turn displayResults false  ", () => {
    var id = { id: "string" };
    wrapper.instance().handleSelect(id);
    expect(wrapper.state().displayResults).toBe(false);
  });

  it("5. handleChange() should turn displayResults true  ", () => {
    var id = { id: "string" };
    wrapper.instance().handleSelect(id);
    expect(wrapper.state().displayResults).toBe(false);

    var t = { value: "0" };
    var event = { target: t };
    wrapper.instance().handleChange(event);
    expect(wrapper.state().displayResults).toBe(true);
  });
});

describe("<Search/> Mount Tests", () => {
  it("1. Result should not display", () => {
    const wrapper = mount(<Search />);

    wrapper
      .find(Search)
      .instance()
      .setState({
        displayResults: false
      });

    wrapper.update();
    expect(wrapper.state().displayResults).toEqual(false);
  });

  it("2. Result should display", () => {
    const wrapper = mount(<Search />);

    wrapper
      .find(Search)
      .instance()
      .setState({
        displayResults: true
      });

    wrapper.update();
    expect(wrapper.state().displayResults).toEqual(true);
  });
});
