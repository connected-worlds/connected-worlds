import React from "react";
import ReactDOM from "react-dom";
import SearchItem from "./SearchItem";
import { shallow } from "enzyme";

describe("<SearchItem/> Tests", () => {
  it("1. Renders SearchItem without crashing", () => {
    var t = { tags: "" };
    const div = document.createElement("div");
    ReactDOM.render(<SearchItem item={t} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it("2. Renders SearchItem correctly without crashing", () => {
    var t = { tags: "" };
    const div = document.createElement("div");
    ReactDOM.render(<SearchItem item={t} />, div);
    expect(div).toMatchSnapshot();
    ReactDOM.unmountComponentAtNode(div);
  });

  var t = { tags: "" };
  const wrapper = shallow(<SearchItem item={t} />);

  it('3. An element with class "ui-menu-item" exists', () => {
    expect(wrapper.find(".ui-menu-item").exists()).toBeTruthy();
  });

  it('4. An element with class "ui-menu-item-wrapper" exists', () => {
    expect(wrapper.find(".ui-menu-item-wrapper").exists()).toBeTruthy();
  });

  it('5. An element with class "tag-show" exists', () => {
    expect(wrapper.find(".tag-show").exists()).toBeTruthy();
  });

  it("6. Contains one span component", () => {
    expect(wrapper.find("li").length).toBe(1);
  });
});
