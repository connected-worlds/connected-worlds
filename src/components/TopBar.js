import React from "react";
import Views from "./Views";
import hamburger from "../assets/hamburger.png";
import close from "../assets/close.png";
import DetailsPane from "./DetailsPane";
import { observer } from "mobx-react";
import "./TopBar.css";
import cytoscapeStore from "../util/CytoscapeStore";

class TopBar extends React.Component {
  constructor() {
    super();
    this.state = {
      open: false
    };
    this.toggleIcon = this.toggleIcon.bind(this);
  }

  render() {
    if (!cytoscapeStore.loadingFinished) {
      return null;
    }
    return (
      <div className="Navbar">
        <div className="Navbar_Item Navbar_Item_Title">VIEW</div>
        <img
          className="Navbar_Item Navbar_Item-toggle"
          src={this.state.open ? close : hamburger}
          alt="Menu"
          id="hamburger"
          width="20"
          height="20"
          onClick={this.toggleIcon}
        />

        <nav className="Navbar_Items">
          <Views />
        </nav>

        <DetailsPane highlightedNode={this.props.highlightedNode} />
      </div>
    );
  }

  toggleIcon() {
    this.setState({
      open: !this.state.open
    });
  }

  componentDidMount() {
    if (document.querySelector(".Navbar_Item-toggle") !== null) {
      document
        .querySelector(".Navbar_Item-toggle")
        .addEventListener("click", this.classToggle);
    }
  }

  classToggle = () => {
    const navs = document.querySelectorAll(".Navbar_Items");

    navs.forEach(nav => nav.classList.toggle("Navbar_ToggleShow"));
  };
}

export default observer(TopBar);
