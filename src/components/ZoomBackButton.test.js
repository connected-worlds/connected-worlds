import React from "react";
import ReactDOM from "react-dom";
import ZoomBackButton from "./ZoomBackButton";
import cytoscapeStore from "../util/CytoscapeStore";
import { shallow } from "enzyme";

describe("<ZoomBackButton /> Tests", () => {
  it("1. Rendering ZoomBackButton without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<ZoomBackButton />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it("2. Renders ZoomBackButton correctly without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<ZoomBackButton />, div);
    expect(div).toMatchSnapshot();
    ReactDOM.unmountComponentAtNode(div);
  });

  it("3. One div tag exists", () => {
    cytoscapeStore.loadingFinished = true;
    const wrapper = shallow(<ZoomBackButton />);
    expect(wrapper.find("div").exists).toBeTruthy;
  });
});
